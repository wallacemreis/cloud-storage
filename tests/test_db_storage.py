import datetime
import random
import string

import pytest

from cloud_storage.storage import get_storage


timestamp = str(datetime.datetime.utcnow())
DUMMY_PAYLOAD = string.ascii_letters.encode("ascii")
RANDOM_FILENAME = "".join(random.sample(string.ascii_letters, 15))
RANDOM_PATH = f"/Test/{timestamp}"
RANDON_PATH_FILENAME = f"/Test/{timestamp}/{RANDOM_FILENAME}"


@pytest.fixture(scope="module")
def sto():
    storage = get_storage("dbx_storage")
    yield storage
    # Cleanup folder
    storage.del_content(RANDOM_PATH)


def test_record_file(sto):
    test_contents = DUMMY_PAYLOAD
    resp = sto.record_file(test_contents, RANDON_PATH_FILENAME)
    expected = True
    assert resp == expected


def test_retrieve_file(sto):
    resp = sto.retrieve_file(RANDON_PATH_FILENAME)
    expected = DUMMY_PAYLOAD.decode("ascii")
    assert expected == resp


def test_list_files(sto):
    expected = [RANDON_PATH_FILENAME]
    resp = sto.list_files(RANDOM_PATH)
    assert expected == resp


def test_del_content(sto):
    expected = True
    test_contents = DUMMY_PAYLOAD
    test_file_name = "".join(random.sample(string.ascii_letters, 15))
    test_file = f"/Test/{timestamp}/{test_file_name}"
    sto.record_file(test_contents, test_file)
    resp = sto.del_content(test_file)
    assert expected == resp
