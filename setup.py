from setuptools import setup, find_packages

setup(
    name="cloud_storage",
    py_module=["plugins", "plugins.dbx", "plugins.dummy"],
    version="0.1",
    description="The package to wraps cloud storage",
    url="https://gitlab.com/wallacemreis/cloud-storage",
    author="Wallace Reis",
    author_email="wallace.mreis@gmail.com",
    license="GNU",
    packages=find_packages(),
    install_requeires=["dropbox"],
    zip_safe=False,
)
