from abc import ABC, abstractmethod, abstractproperty


class AbstractStorage(ABC):
    @abstractproperty
    def token_folder(self):
        raise NotImplementedError

    @abstractmethod
    def record_file(self):
        raise NotImplementedError

    @abstractmethod
    def retrieve_file(self):
        raise NotImplementedError

    @abstractmethod
    def list_files(self):
        raise NotImplementedError
