import logging
from configparser import ConfigParser
from pathlib import Path, PosixPath
from typing import List
import os

import dropbox

from ..provider import AbstractStorage

cur_dir: PosixPath = Path(__file__).parent
CLASS_NAME = "DropBoxStorage"


class DropBoxStorage(AbstractStorage):
    """The successful result of a call to a route."""

    token_folder: str = "/tokens/"
    _parser: ConfigParser = ConfigParser()
    config_file: str = "dropbox.ini"

    def __init__(self):
        self._parser.read(cur_dir.joinpath(self.config_file).absolute())
        # self.dbx = dropbox.Dropbox(str(self._parser.get("dropbox", "ACCESS_TOKEN")))
        self.dbx = dropbox.Dropbox(os.getenv('DB_ACCESS_TOKEN'))

    def retrieve_file(self, filename: str) -> str:
        """
        :param str obj_result: The result of a route not including the binary
            payload portion, if one exists. Must be serialized JSON.
        :param requests.models.Response http_resp: A raw HTTP response. It will
            be used to stream the binary-body payload of the response.
        """
        try:
            _, dbx_file = self.dbx.files_download(filename)
            return dbx_file.text
        except Exception as e:
            logging.error(e)
            return False

    def record_file(self, content: bytes, filename: str) -> bool:
        """
        :param str obj_result: The result of a route not including the binary
            payload portion, if one exists. Must be serialized JSON.
        :param requests.models.Response http_resp: A raw HTTP response. It will
            be used to stream the binary-body payload of the response.
        """
        mode = dropbox.files.WriteMode.overwrite
        try:
            self.dbx.files_upload(content, filename, mode)
            return True
        except Exception as e:
            logging.error(e)
            return False

    def list_files(self, folder: str) -> bool:
        """
        :param str obj_result: The result of a route not including the binary
            payload portion, if one exists. Must be serialized JSON.
        :param requests.models.Response http_resp: A raw HTTP response. It will
            be used to stream the binary-body payload of the response.
        """
        try:
            list_folder: List[str] = [
                dbfile.path_display for dbfile in self.dbx.files_list_folder(folder).entries
            ]
            return list_folder
        except Exception as e:
            logging.error(e)
            return False

    def del_content(self, folder: str) -> bool:
        try:
            self.dbx.files_delete_v2(folder)
            return True
        except Exception as e:
            logging.error(e)
            return False
