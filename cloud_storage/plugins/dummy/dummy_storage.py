from pathlib import Path, PosixPath

from ..provider import AbstractStorage

cur_dir: PosixPath = Path(__file__).parent
CLASS_NAME = "DummyStorage"


class DummyStorage(AbstractStorage):
    """The successful result of a call to a route."""

    token_folder: str = "/dummy/"

    def __init__(self):
        pass

    def retrieve_file(self, filename: str) -> str:
        pass

    def record_file(self, content: bytes, filename: str) -> bool:
        pass

    def list_files(self, folder: str) -> bool:
        pass

    def del_content(self, folder: str) -> bool:
        pass
