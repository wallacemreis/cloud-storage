import logging
from importlib import import_module
from pathlib import Path


logging.basicConfig(level=logging.DEBUG)

registered_plugin = {}
plugin_dir = Path(__file__).absolute().parent / "plugins"


plugins = [
    ".".join(_file.parts[-3:]).replace(".py", "")
    for folder in plugin_dir.iterdir()
    for _file in folder.glob("*.py")
    if not _file.name.startswith("__")
]

for plugin in plugins:
    module = import_module(f".{plugin}", __package__)
    logging.info("Imported --> %s", module)
    registered_plugin.update({module.__name__.split(".")[-1]: module})


def get_storage(storage_name):
    logging.info("Pass plugin: %s", storage_name)
    storage_module = registered_plugin.get(storage_name, "dbx_storage")
    return eval(f"storage_module.{storage_module.CLASS_NAME}()")
